package com.mdm.equipmentservice.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.util.concurrent.ConcurrentHashMap;

@Configuration
@EnableAsync
@EnableMongoRepositories(basePackages = "com.mdm.equipmentservice.model.mongo")
@EnableJpaRepositories(basePackages = "com.mdm.equipmentservice.model.repository")
@EnableAspectJAutoProxy
public class ApplicationConfig {


    /**
     * Map of emitters that shared between threads, each key is a username that represents a user that subscribes to a SseEmitter.
     */
    @Bean
    @Qualifier("pushNotificationEmitters")
    @Primary
    public ConcurrentHashMap<String, SseEmitter> pushNotificationEmitters() {
        return new ConcurrentHashMap<>();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
