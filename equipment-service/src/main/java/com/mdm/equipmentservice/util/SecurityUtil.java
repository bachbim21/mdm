package com.mdm.equipmentservice.util;

import com.mdm.equipmentservice.model.entity.User;
import com.mdm.equipmentservice.model.repository.UserRepository;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;

import java.util.List;

public class SecurityUtil {

    private static User cachedCurrentLoggedInUser = null;

    public static User getCurrentLoggedInUser(UserRepository userRepository) {
        String username = getUsernameOfCurrentLoggedInUser();
        if (cachedCurrentLoggedInUser != null && cachedCurrentLoggedInUser.getUsername().equals(username)) {
            return cachedCurrentLoggedInUser;
        }
        cachedCurrentLoggedInUser = userRepository.findByUsernameIgnoreCase(username).orElseThrow();
        return cachedCurrentLoggedInUser;
    }

    public static String getUsernameOfCurrentLoggedInUser() {
        Jwt principal = (Jwt) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return principal.getClaim("preferred_username");
    }

    public static List<SimpleGrantedAuthority> getCurrentLoggedInUserAuthorities() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getAuthorities().stream().map(SimpleGrantedAuthority.class::cast).toList();
    }

    public static boolean currentUserHasAnyAuthorities(String... authorities) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        for (String authority : authorities) {
            if (authentication.getAuthorities().contains(new SimpleGrantedAuthority(authority))) {
                return true;
            }
        }
        return false;
    }

    public static boolean currentUserHasAuthority(String authority) {
        return currentUserHasAnyAuthorities(authority);
    }

    public static boolean currentUserHasAllAuthorities(String... authorities) {
        for (String authority : authorities) {
            if (!currentUserHasAuthority(authority)) {
                return false;
            }
        }
        return true;
    }

    public static boolean currentUserDoesNotHaveAllAuthorities(String... authorities) {
        for (String authority : authorities) {
            if (currentUserHasAuthority(authority)) {
                return false;
            }
        }
        return true;
    }
}
