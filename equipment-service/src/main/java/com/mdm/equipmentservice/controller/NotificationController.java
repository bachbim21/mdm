package com.mdm.equipmentservice.controller;

import com.mdm.equipmentservice.model.entity.Notification;
import com.mdm.equipmentservice.model.entity.NotificationType;
import com.mdm.equipmentservice.model.mongo.NotificationMongoRepository;
import com.mdm.equipmentservice.query.param.GetNotificationQueryParam;
import com.mdm.equipmentservice.response.GenericResponse;
import com.mdm.equipmentservice.service.NotificationService;
import com.mdm.equipmentservice.util.SecurityUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@RestController
@RequestMapping("/api/v1/notifications")
public class NotificationController {

    private final NotificationService notificationService;

    private final PagedResourcesAssembler<Notification> notificationPagedResourcesAssembler;

    private final NotificationMongoRepository notificationMongoRepository;

    private final ConcurrentHashMap<String, SseEmitter> emitters ;

    @Autowired
    public NotificationController(NotificationService notificationService,
                                  PagedResourcesAssembler<Notification> notificationPagedResourcesAssembler,
                                  NotificationMongoRepository notificationMongoRepository, ConcurrentHashMap<String, SseEmitter> emitters) {
        this.notificationService = notificationService;
        this.notificationPagedResourcesAssembler = notificationPagedResourcesAssembler;
        this.notificationMongoRepository = notificationMongoRepository;
        this.emitters = emitters;
    }

    @GetMapping
    public ResponseEntity<GenericResponse<PagedModel<EntityModel<Notification>>>> getAllNotification(
            GetNotificationQueryParam getNotificationQueryParam,
            Pageable pageable) {
        PagedModel<EntityModel<Notification>> entityModels;
        entityModels = notificationPagedResourcesAssembler.toModel(notificationService.getAllNotification(getNotificationQueryParam, pageable));
        return ResponseEntity.ok(new GenericResponse<>(entityModels));
    }

    /**
     * Push notification to the client by using server-sent event, just like websocket, but websocket is bidirectional communication,
     * server-sent event is unidirectional communication.
     * <br>
     * The client that calls this endpoint will subscribe to a SseEmitter, and will receive notification in realtime.
     */
    @GetMapping("/push")
    public SseEmitter pushNotification() {
        SseEmitter emitter = new SseEmitter();
        String usernameOfCurrentLoggedInUser = SecurityUtil.getUsernameOfCurrentLoggedInUser();
        log.info(
                "User with username: {} is subscribing notification event source. Emitter hashCode: {}. Thread: {}",
                usernameOfCurrentLoggedInUser,
                emitter.hashCode(),
                Thread.currentThread().getName()
        );
        emitters.put(usernameOfCurrentLoggedInUser, emitter);
        emitter.onTimeout(() -> {
            emitters.remove(usernameOfCurrentLoggedInUser);
            emitter.complete();
        });
        emitter.onCompletion(() -> {
            emitters.remove(usernameOfCurrentLoggedInUser);
            emitter.complete();
        });
        return emitter;
    }

    @GetMapping("/save-test")
    public ResponseEntity<?> saveSomeTestNotification(String content) {
        Notification notification = new Notification();
        notification.setContent(content);
        notification.setNotificationType(NotificationType.HANDOVER);
        notification.setCreatedAt(LocalDateTime.now());
        notificationMongoRepository.save(notification);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<GenericResponse<Object>> deleteNotification(@PathVariable(name = "id") String id) {
        notificationService.deleteNotification(id);
        return ResponseEntity.ok(new GenericResponse<>(null));
    }
}
