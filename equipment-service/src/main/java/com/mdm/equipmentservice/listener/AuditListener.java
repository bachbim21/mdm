package com.mdm.equipmentservice.listener;

import com.mdm.equipmentservice.model.entity.AuditMetadata;
import com.mdm.equipmentservice.model.entity.trait.Auditable;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;

import java.time.LocalDateTime;

public class AuditListener {

    @PrePersist
    public void setCreatedAt(Object entity) {
        if (entity instanceof Auditable) {
            Auditable auditable = (Auditable) entity;
            AuditMetadata auditMetadata = auditable.getAuditMetadata();

            if (auditMetadata == null) {
                auditMetadata = new AuditMetadata();
                auditable.setAuditMetadata(auditMetadata);
            }

            auditMetadata.setCreatedAt(LocalDateTime.now());
            auditMetadata.setUpdatedAt(LocalDateTime.now());
        }

    }

    @PreUpdate
    public void setUpdatedAt(Object entity) {
        if (entity instanceof Auditable) {
            Auditable auditable = (Auditable) entity;
            AuditMetadata auditMetadata = auditable.getAuditMetadata();
            auditMetadata.setUpdatedAt(LocalDateTime.now());
        }
    }
}

