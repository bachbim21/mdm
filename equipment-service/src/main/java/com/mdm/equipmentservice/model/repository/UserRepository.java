package com.mdm.equipmentservice.model.repository;

import com.mdm.equipmentservice.model.dto.base.UserDto;
import com.mdm.equipmentservice.model.entity.NotificationType;
import com.mdm.equipmentservice.model.entity.User;
import com.querydsl.core.types.Predicate;
import jakarta.validation.constraints.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface UserRepository extends ParentRepository<User, Long> {

    List<User> findByRoleNameInIgnoreCase(Collection<String> roleNames);

    Optional<User> findByEmailIgnoreCase(String email);

    @EntityGraph(value = "userWithRole")
    Optional<User> findByUsernameIgnoreCase(String username);

    @Override
    @EntityGraph(value = "userFullInfo")
    @NotNull
    Page<User> findAll(@NotNull Predicate predicate, @NotNull Pageable pageable);

    @Query("select distinct u from User u inner join u.role.notificationConfigs notificationConfigs where notificationConfigs.notificationType = ?1")
    Set<User> findDistinctByRole_NotificationConfigs_NotificationType(NotificationType notificationType);

}