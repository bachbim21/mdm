package com.mdm.equipmentservice.model.entity.trait;

import com.mdm.equipmentservice.model.entity.AuditMetadata;

public interface Auditable {
    AuditMetadata getAuditMetadata();
    void setAuditMetadata(AuditMetadata auditMetadata);

}
