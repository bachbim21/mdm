package com.mdm.equipmentservice.model.entity;


import com.mdm.equipmentservice.listener.AuditListener;
import com.mdm.equipmentservice.model.entity.trait.Auditable;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
@Table(name = "equipment_units", uniqueConstraints = {@UniqueConstraint(columnNames = {"name"}, name = "name")})
@EntityListeners(AuditListener.class)
public class EquipmentUnit implements Auditable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(columnDefinition = "TEXT")
    private String note;

    @Embedded
    private AuditMetadata auditMetadata;
}
