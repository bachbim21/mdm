package com.mdm.equipmentservice.aspect;

import com.mdm.equipmentservice.model.entity.Notification;
import com.mdm.equipmentservice.service.NotificationService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class PushNotificationAspect {

    private final NotificationService notificationService;

    public PushNotificationAspect(NotificationService notificationService) {
        this.notificationService = notificationService;
    }


    @Pointcut("execution(* com.mdm.equipmentservice.model.mongo.NotificationMongoRepository.save(..))")
    private void onSaveNotification() {
    }

    @AfterReturning(pointcut = "onSaveNotification()", returning = "notification")
    public void pushNotificationUsingEmitter(Notification notification) {
        notificationService.pushNotificationThroughSseEmitter(notification);
    }
}
