package com.mdm.equipmentservice.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyEmitter;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class NotificationServiceV2 {

    private final List<ResponseBodyEmitter> emitters = new ArrayList<>();

    public void addEmitter(ResponseBodyEmitter emitter) {
        emitters.add(emitter);
    }

    public void sendNotification(String message) {
        log.info("sending notification: {}", message);
        // Send the notification message to all registered emitters
        List<ResponseBodyEmitter> deadEmitters = new ArrayList<>();
        emitters.forEach(emitter -> {
            try {
                emitter.send(message);
            } catch (Exception e) {
                // Handle exceptions (e.g., client disconnects)
                deadEmitters.add(emitter);
            }
        });

        // Remove dead emitters
        emitters.removeAll(deadEmitters);
    }

}
