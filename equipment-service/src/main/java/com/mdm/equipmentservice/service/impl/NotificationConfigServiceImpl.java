package com.mdm.equipmentservice.service.impl;

import com.mdm.equipmentservice.mapper.NotificationConfigMapper;
import com.mdm.equipmentservice.model.dto.form.UpdateNotificationConfigForm;
import com.mdm.equipmentservice.model.dto.fullinfo.NotificationConfigFullInfoDto;
import com.mdm.equipmentservice.model.entity.NotificationConfig;
import com.mdm.equipmentservice.model.entity.NotificationType;
import com.mdm.equipmentservice.model.entity.User;
import com.mdm.equipmentservice.model.repository.NotificationConfigRepository;
import com.mdm.equipmentservice.model.repository.UserRepository;
import com.mdm.equipmentservice.service.NotificationConfigService;
import com.mdm.equipmentservice.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
@Slf4j
public class NotificationConfigServiceImpl implements NotificationConfigService {

    private final UserRepository userRepository;

    private final NotificationConfigMapper notificationConfigMapper;

    private final NotificationConfigRepository notificationConfigRepository;

    private final UserService userService;

    @Autowired
    public NotificationConfigServiceImpl(NotificationConfigMapper notificationConfigMapper, NotificationConfigRepository notificationConfigRepository,
                                         UserService userService,
                                         UserRepository userRepository) {
        this.notificationConfigMapper = notificationConfigMapper;
        this.notificationConfigRepository = notificationConfigRepository;
        this.userService = userService;
        this.userRepository = userRepository;
    }


    @Override
    public void updateNotificationConfig(Set<UpdateNotificationConfigForm> updateNotificationConfigForms) {
        notificationConfigRepository.deleteAll();
        List<NotificationConfig> notificationConfigs = updateNotificationConfigForms.stream().map(notificationConfigMapper::toEntity).toList();
        notificationConfigRepository.saveAll(notificationConfigs);
    }

    @Override
    public List<NotificationConfigFullInfoDto> getNotificationConfigs() {
        return notificationConfigRepository.findAll().stream().map(notificationConfigMapper::toFullInfoDto).toList();
    }

    @Override
    @Cacheable(value = "usersForNotificationType")
    public Set<User> getUsersForNotificationType(NotificationType notificationType) {
        return userRepository.findDistinctByRole_NotificationConfigs_NotificationType(notificationType);
    }

}
