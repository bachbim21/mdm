package com.mdm.equipmentservice.service.impl;

import com.mdm.equipmentservice.event.*;
import com.mdm.equipmentservice.mapper.*;
import com.mdm.equipmentservice.model.entity.*;
import com.mdm.equipmentservice.model.mongo.NotificationMongoRepository;
import com.mdm.equipmentservice.query.param.GetNotificationQueryParam;
import com.mdm.equipmentservice.query.predicate.NotificationPredicate;
import com.mdm.equipmentservice.service.NotificationConfigService;
import com.mdm.equipmentservice.service.NotificationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Service
@Slf4j
public class NotificationServiceImpl implements NotificationService {

    private final NotificationMongoRepository notificationMongoRepository;

    private final HandoverTicketMapper handoverTicketMapper;

    private final ReportBrokenTicketMapper reportBrokenTicketMapper;

    private final InspectionTicketMapper inspectionTicketMapper;

    private final MaintenanceTicketMapper maintenanceTicketMapper;

    private final LiquidationTicketMapper liquidationTicketMapper;

    private final RepairTicketMapper repairTicketMapper;

    private final TransferTicketMapper transferTicketMapper;

    private final ConcurrentHashMap<String, SseEmitter> pushNotificationEmitters;

    private final NotificationConfigService notificationConfigService;

    public NotificationServiceImpl(NotificationMongoRepository notificationMongoRepository, HandoverTicketMapper handoverTicketMapper,
                                   ReportBrokenTicketMapper reportBrokenTicketMapper, InspectionTicketMapper inspectionTicketMapper,
                                   MaintenanceTicketMapper maintenanceTicketMapper, LiquidationTicketMapper liquidationTicketMapper,
                                   RepairTicketMapper repairTicketMapper, TransferTicketMapper transferTicketMapper,
                                   ConcurrentHashMap<String, SseEmitter> pushNotificationEmitters,
                                   NotificationConfigService notificationConfigService) {
        this.notificationMongoRepository = notificationMongoRepository;
        this.handoverTicketMapper = handoverTicketMapper;
        this.reportBrokenTicketMapper = reportBrokenTicketMapper;
        this.inspectionTicketMapper = inspectionTicketMapper;
        this.maintenanceTicketMapper = maintenanceTicketMapper;
        this.liquidationTicketMapper = liquidationTicketMapper;
        this.repairTicketMapper = repairTicketMapper;
        this.transferTicketMapper = transferTicketMapper;
        this.pushNotificationEmitters = pushNotificationEmitters;
        this.notificationConfigService = notificationConfigService;
    }

    @Override
    public Page<Notification> getAllNotification(GetNotificationQueryParam getNotificationQueryParam, Pageable pageable) {
        return notificationMongoRepository.findAll(NotificationPredicate.getNotificationPredicate(getNotificationQueryParam), pageable);
    }

    @Override
    public void deleteNotification(String id) {
        notificationMongoRepository.deleteById(id);
    }

    @Override
    public void pushNotificationThroughSseEmitter(Notification notification) {
        Set<User> usersCanReceiveNotification = notificationConfigService.getUsersForNotificationType(notification.getNotificationType());
        Set<String> usernamesCanReceiveNotification = extractUsernameOfUsers(usersCanReceiveNotification);

        for (String username : usernamesCanReceiveNotification) {
            if (isUsernameSubscribedToPushNotification(username, pushNotificationEmitters)) {
                SseEmitter emitter = pushNotificationEmitters.get(username);
                pushNotification(notification, username, emitter);
            }
        }
    }

    private void pushNotification(Notification notification, String username, SseEmitter emitter) {
        try {
            int emitterHashCode = emitter.hashCode();
            log.info("Sending notification to user: {}, notification content: {}", username, emitterHashCode);
            emitter.send(notification);
        } catch (IOException e) {
            int emitterHashCode = emitter.hashCode();
            log.info("Send notification to user: {} failed, removing emitter: {}", username, emitterHashCode);
            pushNotificationEmitters.remove(username);
        }
    }

    private boolean isUsernameSubscribedToPushNotification(String username, ConcurrentHashMap<String, SseEmitter> pushNotificationEmitters) {
        return pushNotificationEmitters.containsKey(username);
    }

    private Set<String> extractUsernameOfUsers(Set<User> usersCanReceiveNotification) {
        return usersCanReceiveNotification.stream().map(User::getUsername).collect(Collectors.toSet());
    }


    @Override
    @EventListener
    @Async
    @Order
    public void createNotification(HandoverTicketCreatedEvent handoverTicketCreatedEvent) {
        HandoverTicket handover = handoverTicketCreatedEvent.getHandoverTicket();
        Notification notification = new Notification();
        notification.setNotificationType(NotificationType.HANDOVER);
        notification.setCreatedAt(LocalDateTime.now());
        notification.setContent(String.format("""
                Phiếu bàn giao thiết bị %s đã được tạo.
                """, handover.getEquipment().getName()));
        notification.setEquipmentId(handover.getEquipment().getId());
        notification.setEvent(handoverTicketMapper.toEventDto(handoverTicketCreatedEvent));
        notificationMongoRepository.save(notification);
    }

    @Override
    @EventListener
    @Async
    @Order
    public void createNotification(HandoverTicketAcceptedEvent handoverAcceptedEvent) {
        HandoverTicket handover = handoverAcceptedEvent.getHandoverTicket();
        Equipment equipment = handover.getEquipment();
        Department department = handover.getDepartment();
        Notification notification = new Notification();
        notification.setNotificationType(NotificationType.HANDOVER);
        notification.setCreatedAt(LocalDateTime.now());
        String status = handover.getStatus().equals(TicketStatus.ACCEPTED) ? "được chấp nhận" : "bị từ chối";
        notification.setContent(String.format("""
                Thiết bị %s đã %s bàn giao qua %s.
                """, equipment.getName(), status, department.getName()));
        notification.setEquipmentId(equipment.getId());
        notification.setEvent(handoverTicketMapper.toEventDto(handoverAcceptedEvent));
        notificationMongoRepository.save(notification);
    }

    @Override
    @EventListener
    @Async
    @Order
    public void createNotification(ReportBrokenTicketCreatedEvent brokenReportTicketCreatedEvent) {
        ReportBrokenTicket brokenReportTicket = brokenReportTicketCreatedEvent.getReportBrokenTicket();
        Equipment equipment = brokenReportTicket.getEquipment();
        Notification notification = new Notification();
        notification.setNotificationType(NotificationType.REPORT_BROKEN);
        notification.setCreatedAt(LocalDateTime.now());
        notification.setContent(String.format("""
                Phiếu báo hỏng thiết bị %s đã được tạo. Mức độ quan trọng: %s
                """, equipment.getName(), brokenReportTicket.getPriority().toString()));
        notification.setEquipmentId(equipment.getId());
        notification.setEvent(reportBrokenTicketMapper.toEventDto(brokenReportTicketCreatedEvent));
        notificationMongoRepository.save(notification);
    }

    @Override
    @EventListener
    @Async
    @Order
    public void createNotification(ReportBrokenTicketAcceptedEvent brokenReportTicketAcceptedEvent) {
        ReportBrokenTicket brokenReportTicket = brokenReportTicketAcceptedEvent.getReportBrokenTicket();
        Equipment equipment = brokenReportTicket.getEquipment();
        Notification notification = new Notification();
        notification.setNotificationType(NotificationType.REPORT_BROKEN);
        notification.setCreatedAt(LocalDateTime.now());
        String status = brokenReportTicket.getStatus().equals(TicketStatus.ACCEPTED) ? "được báo hỏng" : "bị từ chối báo hỏng";
        notification.setContent(String.format("""
                Thiết bị %s đã được %s. Mức độ quan trọng: %s
                """, equipment.getName(), status, brokenReportTicket.getPriority().toString()));
        notification.setEquipmentId(equipment.getId());
        notification.setEvent(reportBrokenTicketMapper.toEventDto(brokenReportTicketAcceptedEvent));
        notificationMongoRepository.save(notification);
    }

    @Override
    @EventListener
    @Async
    @Order
    public void createNotification(InspectionTicketCreatedEvent inspectionTicketCreatedEvent) {
        InspectionTicket inspectionTicket = inspectionTicketCreatedEvent.getInspectionTicket();
        Equipment equipment = inspectionTicket.getEquipment();
        Notification notification = new Notification();
        notification.setNotificationType(NotificationType.INSPECTION);
        notification.setCreatedAt(LocalDateTime.now());
        notification.setContent(String.format("""
                Phiếu kiểm định thiết bị %s đã được tạo .
                """, equipment.getName()));
        notification.setEquipmentId(equipment.getId());
        notification.setEvent(inspectionTicketMapper.toEventDto(inspectionTicketCreatedEvent));
        notificationMongoRepository.save(notification);
    }

    @Override
    @EventListener
    @Async
    @Order
    public void createNotification(InspectionTicketUpdatedEvent inspectionTicketUpdatedEvent) {
        InspectionTicket inspectionTicket = inspectionTicketUpdatedEvent.getInspectionTicket();
        Equipment equipment = inspectionTicket.getEquipment();
        Notification notification = new Notification();
        notification.setNotificationType(NotificationType.INSPECTION);
        notification.setCreatedAt(LocalDateTime.now());
        notification.setContent(String.format("""
                Phiếu kiểm định thiết bị %s đã được cập nhật.
                """, equipment.getName()));
        notification.setEquipmentId(equipment.getId());
        notification.setEvent(inspectionTicketMapper.toEventDto(inspectionTicketUpdatedEvent));
        notificationMongoRepository.save(notification);
    }

    @Override
    @EventListener
    @Async
    @Order
    public void createNotification(InspectionTicketAcceptedEvent inspectionTicketAcceptedEvent) {
        InspectionTicket inspectionTicket = inspectionTicketAcceptedEvent.getInspectionTicket();
        Equipment equipment = inspectionTicket.getEquipment();
        Notification notification = new Notification();
        notification.setNotificationType(NotificationType.INSPECTION);
        notification.setCreatedAt(LocalDateTime.now());
        String status = inspectionTicket.getStatus().equals(TicketStatus.ACCEPTED) ? "được tạo lịch kiểm định" : "bị từ chối tạo lịch kiểm định";
        notification.setContent(String.format("""
                Thiết bị %s đã %s.
                """, equipment.getName(), status));
        notification.setEquipmentId(equipment.getId());
        notification.setEvent(inspectionTicketMapper.toEventDto(inspectionTicketAcceptedEvent));
        notificationMongoRepository.save(notification);
    }

    @Override
    @EventListener
    @Async
    @Order
    public void createNotification(MaintenanceTicketCreatedEvent maintenanceTicketCreatedEvent) {
        MaintenanceTicket maintenanceTicket = maintenanceTicketCreatedEvent.getMaintenanceTicket();
        Equipment equipment = maintenanceTicket.getEquipment();
        Notification notification = new Notification();
        notification.setNotificationType(NotificationType.MAINTENANCE);
        notification.setCreatedAt(LocalDateTime.now());
        notification.setContent(String.format("""
                Phiếu bảo trì thiết bị %s đã được tạo.
                """, equipment.getName()));
        notification.setEquipmentId(equipment.getId());
        notification.setEvent(maintenanceTicketMapper.toEventDto(maintenanceTicketCreatedEvent));
        notificationMongoRepository.save(notification);
    }

    @Override
    @EventListener
    @Async
    @Order
    public void createNotification(MaintenanceTicketUpdatedEvent maintenanceTicketUpdatedEvent) {
        MaintenanceTicket maintenanceTicket = maintenanceTicketUpdatedEvent.getMaintenanceTicket();
        Equipment equipment = maintenanceTicket.getEquipment();
        Notification notification = new Notification();
        notification.setNotificationType(NotificationType.MAINTENANCE);
        notification.setCreatedAt(LocalDateTime.now());
        notification.setContent(String.format("""
                Phiếu bảo trì thiết bị %s đã được cập nhật.
                """, equipment.getName()));
        notification.setEquipmentId(equipment.getId());
        notification.setEvent(maintenanceTicketMapper.toEventDto(maintenanceTicketUpdatedEvent));
        notificationMongoRepository.save(notification);
    }

    @Override
    @EventListener
    @Async
    @Order
    public void createNotification(MaintenanceTicketAcceptedEvent maintenanceTicketAcceptedEvent) {
        MaintenanceTicket maintenanceTicket = maintenanceTicketAcceptedEvent.getMaintenanceTicket();
        Equipment equipment = maintenanceTicket.getEquipment();
        Notification notification = new Notification();
        notification.setNotificationType(NotificationType.MAINTENANCE);
        notification.setCreatedAt(LocalDateTime.now());
        String status = maintenanceTicket.getStatus().equals(TicketStatus.ACCEPTED) ? "được tạo lịch bảo trì" : "bị từ chối tạo lịch bảo trì";
        notification.setContent(String.format("""
                Thiết bị %s đã %s.
                """, equipment.getName(), status));
        notification.setEquipmentId(equipment.getId());
        notification.setEvent(maintenanceTicketMapper.toEventDto(maintenanceTicketAcceptedEvent));
        notificationMongoRepository.save(notification);
    }

    @Override
    @EventListener
    @Async
    @Order
    public void createNotification(LiquidationTicketCreatedEvent maintenanceTicketCreatedEvent) {
        LiquidationTicket liquidationTicket = maintenanceTicketCreatedEvent.getLiquidationTicket();
        Equipment equipment = liquidationTicket.getEquipment();
        Notification notification = new Notification();
        notification.setNotificationType(NotificationType.LIQUIDATION);
        notification.setCreatedAt(LocalDateTime.now());
        notification.setContent(String.format("""
                Phiếu thanh lý thiết bị %s đã được tạo.
                """, equipment.getName()));
        notification.setEquipmentId(equipment.getId());
        notification.setEvent(liquidationTicketMapper.toEventDto(maintenanceTicketCreatedEvent));
        notificationMongoRepository.save(notification);
    }

    @Override
    @EventListener
    @Async
    @Order
    public void createNotification(LiquidationTicketAcceptedEvent maintenanceTicketAcceptedEvent) {
        LiquidationTicket liquidationTicket = maintenanceTicketAcceptedEvent.getLiquidationTicket();
        Equipment equipment = liquidationTicket.getEquipment();
        Notification notification = new Notification();
        notification.setNotificationType(NotificationType.LIQUIDATION);
        notification.setCreatedAt(LocalDateTime.now());
        String status = liquidationTicket.getStatus().equals(TicketStatus.ACCEPTED) ? "được thanh lý" : "bị từ chối thanh lý";
        notification.setContent(String.format("""
                Thiết bị %s đã %s.
                """, equipment.getName(), status));
        notification.setEquipmentId(equipment.getId());
        notification.setEvent(liquidationTicketMapper.toEventDto(maintenanceTicketAcceptedEvent));
        notificationMongoRepository.save(notification);
    }

    @Override
    @EventListener
    @Async
    @Order
    public void createNotification(RepairTicketCreatedEvent repairTicketCreatedEvent) {
        RepairTicket repairTicket = repairTicketCreatedEvent.getRepairTicket();
        Equipment equipment = repairTicket.getEquipment();
        Notification notification = new Notification();
        notification.setNotificationType(NotificationType.REPAIR);
        notification.setCreatedAt(LocalDateTime.now());
        notification.setContent(String.format("""
                Phiếu sửa chữa thiết bị %s đã được tạo.
                """, equipment.getName()));
        notification.setEquipmentId(equipment.getId());
        notification.setEvent(repairTicketMapper.toEventDto(repairTicketCreatedEvent));
        notificationMongoRepository.save(notification);
    }

    @Override
    @EventListener
    @Async
    @Order
    public void createNotification(RepairTicketUpdatedEvent repairTicketUpdatedEvent) {
        RepairTicket repairTicket = repairTicketUpdatedEvent.getRepairTicket();
        Equipment equipment = repairTicket.getEquipment();
        Notification notification = new Notification();
        notification.setNotificationType(NotificationType.REPAIR);
        notification.setCreatedAt(LocalDateTime.now());
        notification.setContent(String.format("""
                Phiếu sửa chữa thiết bị %s đã được cập nhật.
                """, equipment.getName()));
        notification.setEquipmentId(equipment.getId());
        notification.setEvent(repairTicketMapper.toEventDto(repairTicketUpdatedEvent));
        notificationMongoRepository.save(notification);
    }

    @Override
    @EventListener
    @Async
    @Order
    public void createNotification(RepairTicketAcceptedEvent repairTicketAcceptedEvent) {
        RepairTicket repairTicket = repairTicketAcceptedEvent.getRepairTicket();
        Equipment equipment = repairTicket.getEquipment();
        Notification notification = new Notification();
        notification.setNotificationType(NotificationType.REPAIR);
        notification.setCreatedAt(LocalDateTime.now());
        String status = repairTicket.getStatus().equals(TicketStatus.ACCEPTED) ? "được tạo lịch sửa chữa" : "bị từ chối tạo lịch sửa chữa";
        notification.setContent(String.format("""
                Thiết bị %s đã %s.
                """, equipment.getName(), status));
        notification.setEquipmentId(equipment.getId());
        notification.setEvent(repairTicketMapper.toEventDto(repairTicketAcceptedEvent));
        notificationMongoRepository.save(notification);
    }

    @Override
    @EventListener
    @Async
    @Order
    public void createNotification(RepairTicketAcceptanceTestingEvent repairTicketAcceptanceTestingEvent) {
        RepairTicket repairTicket = repairTicketAcceptanceTestingEvent.getRepairTicket();
        Equipment equipment = repairTicket.getEquipment();
        Notification notification = new Notification();
        notification.setNotificationType(NotificationType.REPAIR);
        notification.setCreatedAt(LocalDateTime.now());
        String status = repairTicket.getRepairStatus().equals(RepairStatus.DONE) ? "sửa chữa xong, tình trạng sử dụng tốt. " :
                "không thể sửa chữa, đã đuợc chuyển vào kho thanh lý";
        notification.setContent(String.format("""
                Thiết bị %s đã %s.
                """, equipment.getName(), status));
        notification.setEquipmentId(equipment.getId());
        notification.setEvent(repairTicketMapper.toEventDto(repairTicketAcceptanceTestingEvent));
        notificationMongoRepository.save(notification);
    }

    @Override
    @EventListener
    @Async
    @Order
    public void createNotification(TransferTicketCreatedEvent transferTicketCreatedEvent) {
        TransferTicket transferTicket = transferTicketCreatedEvent.getTransferTicket();
        Equipment equipment = transferTicket.getEquipment();
        Notification notification = new Notification();
        notification.setNotificationType(NotificationType.TRANSFER);
        notification.setCreatedAt(LocalDateTime.now());
        notification.setContent(String.format("""
                Phiếu chuyển thiết bị %s đã được tạo.
                """, equipment.getName()));
        notification.setEquipmentId(equipment.getId());
        notification.setEvent(transferTicketMapper.toEventDto(transferTicketCreatedEvent));
        notificationMongoRepository.save(notification);
    }


}
