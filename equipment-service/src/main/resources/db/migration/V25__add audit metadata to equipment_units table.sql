ALTER TABLE equipment_units
    ADD created_at datetime NULL;

ALTER TABLE equipment_units
    ADD updated_at datetime NULL;